# Copyright (C) 2017  Jamie McClymont, Rhys Davies, and Nick Webster
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
from mytimetable.key_dates_scraper import scrape


class KeyDatesGetter:
    def fetch_key_dates(url):
        return scrape(requests.get(url).text)

    def load_key_dates(filename):
        with open(filename)as f:
            return scrape(f.read())

    def __init__(self, from_):
        if from_ == 'download':
            url = 'https://www.victoria.ac.nz/students/study/dates'
            self.key_dates = KeyDatesGetter.fetch_key_dates(url)
        else:
            self.key_dates = KeyDatesGetter.load_key_dates(from_)
