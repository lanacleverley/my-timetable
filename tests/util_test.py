# Copyright (C) 2017  Jamie McClymont, Rhys Davies, and Nick Webster
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from mytimetable import util
from mytimetable.key_dates_getter import KeyDatesGetter
from mytimetable.determine_current_trimester import determine_current_trimester
from datetime import date, timedelta


class TestDateUtils:
    def test_iso_to_gregorian(self):
        start_date = date(2000, 1, 1)
        end_date = date(2100, 1, 1)
        while start_date != end_date:
            start = start_date
            iso = start.isocalendar()
            final = util.iso_to_date(*iso)
            assert start == final
            start_date += timedelta(days=1)


class TestTrimesterUtils:
    def test_determine_trimester(self):
        test_values = [(date(2017, 2, 15), 1),
                       (date(2017, 3, 6), 1),
                       (date(2017, 7, 5), 1),
                       (date(2017, 7, 6), 2),
                       (date(2017, 7, 17), 2),
                       (date(2017, 8, 15), 2),
                       (date(2017, 11, 19), 2),
                       (date(2017, 11, 20), 3),
                       (date(2017, 12, 15), 3)]

        key_dates = KeyDatesGetter('tests/samples/keydates.html').key_dates
        for value in test_values:
            assert determine_current_trimester(key_dates, value[0]) == value[1]
